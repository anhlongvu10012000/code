import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FromtongComponent } from './fromtong.component';

describe('FromtongComponent', () => {
  let component: FromtongComponent;
  let fixture: ComponentFixture<FromtongComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FromtongComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FromtongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
