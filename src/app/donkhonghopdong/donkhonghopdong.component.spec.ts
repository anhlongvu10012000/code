import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DonkhonghopdongComponent } from './donkhonghopdong.component';

describe('DonkhonghopdongComponent', () => {
  let component: DonkhonghopdongComponent;
  let fixture: ComponentFixture<DonkhonghopdongComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DonkhonghopdongComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DonkhonghopdongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
