import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DonhopdongComponent } from './donhopdong.component';

describe('DonhopdongComponent', () => {
  let component: DonhopdongComponent;
  let fixture: ComponentFixture<DonhopdongComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DonhopdongComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DonhopdongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
