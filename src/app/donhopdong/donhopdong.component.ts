import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-donhopdong',
  templateUrl: './donhopdong.component.html',
  styleUrls: ['./donhopdong.component.css']
})
export class DonhopdongComponent implements OnInit {
  formHopDong: FormGroup;
  constructor(private readonly fb: FormBuilder) {
    this.formHopDong = this.fb.group({
      contractId: ["", Validators.required],
      areaId: ["", Validators.required],
      productId: ["", Validators.required],
      shipPointId: ["", Validators.required],
      priceId: ["", Validators.required],
      rmooc: [""],
      driverName: ["", Validators.required],
      vehicle: ["", Validators.required],
      transportMethodId: ["", Validators.required],
      quantity2: ["", Validators.required],
      unitPrice: ["", Validators.required],
      currencyCode: ["", Validators.required],
      description: [""],
      checked: [true],
      locationCode: [""],
      productionLine: [""],
      weightLimit: [null]
    })
  }
  ngOnInit(): void {
  }
  onSubmit(){
    console.log(this.formHopDong.value);
  }
  checked = false;
  dateFormat = 'dd/MM/yyyy'
}
