import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DonkhuyenmaiComponent } from './donkhuyenmai.component';

describe('DonkhuyenmaiComponent', () => {
  let component: DonkhuyenmaiComponent;
  let fixture: ComponentFixture<DonkhuyenmaiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DonkhuyenmaiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DonkhuyenmaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
