import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FromComponent } from './from/from.component';
import { FromtongComponent } from './fromtong/fromtong.component';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { ButonComponent } from './buton/buton.component';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { CeativeOrderComponent } from './ceative-order/ceative-order.component';
import { ThongtinkhachhangComponent } from './thongtinkhachhang/thongtinkhachhang.component';
import { ChitiethoadonComponent } from './chitiethoadon/chitiethoadon.component';
import { DonhopdongComponent } from './donhopdong/donhopdong.component';
import { DonkhonghopdongComponent } from './donkhonghopdong/donkhonghopdong.component';
import { DonkhuyenmaiComponent } from './donkhuyenmai/donkhuyenmai.component';
import { DonsanphammoiComponent } from './donsanphammoi/donsanphammoi.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTabsModule } from 'ng-zorro-antd/tabs';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    FromComponent,
    FromtongComponent,
    ButonComponent,
    CeativeOrderComponent,
    ThongtinkhachhangComponent,
    ChitiethoadonComponent,
    DonhopdongComponent,
    DonkhonghopdongComponent,
    DonkhuyenmaiComponent,
    DonsanphammoiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzCollapseModule,
    ReactiveFormsModule,
    NzSelectModule,
    NzCheckboxModule,
    NzGridModule,
    NzDatePickerModule,
    NzTableModule,
    NzTabsModule,

  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule { }
