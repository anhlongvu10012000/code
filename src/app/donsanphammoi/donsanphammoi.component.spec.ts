import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DonsanphammoiComponent } from './donsanphammoi.component';

describe('DonsanphammoiComponent', () => {
  let component: DonsanphammoiComponent;
  let fixture: ComponentFixture<DonsanphammoiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DonsanphammoiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DonsanphammoiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
